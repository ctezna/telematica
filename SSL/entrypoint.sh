#!/bin/bash

# Start nginx service
service nginx start

# Sleep to wait WAF
sleep 5m

# Create certificate folder
mkdir /Certificate

export DOMAIN="ctezna.site"

# Generate SSL Certificate and conver to PFX
certbot certonly --rsa-key-size 4096  --noninteractive --webroot --agree-tos --register-unsafely-without-email -d $DOMAIN -w /usr/share/nginx/html >> /usr/share/nginx/html/logs_cerbot.html 2>&1
openssl pkcs12 -export -out /Certificate/$DOMAIN.pfx -inkey /etc/letsencrypt/live/$DOMAIN/privkey.pem -in /etc/letsencrypt/live/$DOMAIN/cert.pem -certfile /etc/letsencrypt/live/$DOMAIN/chain.pem -passout pass:Test123 > /usr/share/nginx/html/logs_openssl.html 2>&1
zip $DOMAIN.zip -r /Certificate

#Upload SSL Certificate file


tail -f /dev/null